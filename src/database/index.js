const { Sequelize } = require("sequelize");
const dbConfig = require("../config/dbconfig");

// Import models
const User = require("../models/User");

// Connects to the database useding the configuration file.
const connection = new Sequelize(dbConfig);

// Instantiates the models
User.init(connection);

module.exports = connection;
