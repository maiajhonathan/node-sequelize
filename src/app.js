const express = require("express");
// Makes dbconnection available to be used.
require("./database");
const userRoutes = require("./routes/user");

// Sets a default port in case process.env.PORT is empty.
const PORT = process.env.PORT || 5000;

// Creates an express application.
const app = express();

// middleware
app.use(express.json());

// default route
app.get("/", (req, res) => {
  res.send({ greetings: "Hey there!" });
});

// other routes
app.use(userRoutes);

// Starts the express app and logs a message into the console.
app.listen(PORT, () => {
  console.log(`Server running on port: ${PORT}`);
});
