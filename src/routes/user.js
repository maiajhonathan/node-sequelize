const { Router } = require("express");
const userController = require("../controllers/UserController");

const router = Router();

router.get("/users", userController.index);
router.post("/users", userController.store);

module.exports = router;
