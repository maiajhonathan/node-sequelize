const User = require("../models/User");

module.exports = {
  async index(req, res) {
    const users = await User.findAll();
    res.json(users);
  },
  async store(req, res) {
    // Gets the users information from the request body.
    const { name, password, email } = req.body;

    // Creates a new register in the database.
    const user = await User.create({ name, password, email });

    // Returns the newly created register as a Json response.
    return res.json(user);
  },
};
