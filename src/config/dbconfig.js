module.exports = {
  username: "test",
  password: "123456",
  database: "nodesequelize",
  host: "localhost",
  dialect: "postgres",
  define: {
    timestamps: true,
    underscored: true,
  },
};
